//
//  V2HomeViewController.swift
//  POC-
//
//  Created by Cleber Magalhaes Dias on 29/11/17.
//  Copyright © 2017 CI&T. All rights reserved.
//

import UIKit
import RSLoadingView

class MaterialViewController: UIViewController {

    private let kITHomeViewControllerLoadingSize = CGSize(width: 60, height: 60)
    private let kITHomeViewControllerLoadingSpeed = 2
    
    private var loadingView: LoadingView?
    private var isError = false
    
    private let business = HomeBusiness()

    let button = UIButton(type: UIButtonType.system) as UIButton
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getButton(type: .accent)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    func getButton(type: ButtonType) {
        showLoader()
        business.getMaterial(completion: { button in
            self.applyLayout(type: type, layout: button)
            self.hideLoader()
        })
    }

    func applyLayout(type: ButtonType, layout: CButton) {
        button.removeFromSuperview()
        // set the frame
        button.frame = CGRect(x: 100, y: 100, width: 100, height: 50)
        // button title
        button.setTitle("Button", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.clipsToBounds = true
        button.layer.borderWidth = 0.5

        switch type {
        case .accent:

            button.layer.cornerRadius = CGFloat(layout.accent.block.base.borderRadius)
            button.layer.borderColor = UIColor.init(hexString: layout.accent.block.interactivity.rest.backgroundColor).cgColor
            button.backgroundColor = UIColor(hexString: layout.accent.block.interactivity.rest.backgroundColor)

            let myNormalAttributedTitle = NSAttributedString(string: "Button",
                                                             attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: CGFloat(layout.accent.text.base.fontSize)),
                                                             NSAttributedString.Key.foregroundColor: UIColor(hexString: layout.accent.text.interactivity.rest.fontColor)])

            // add button on view
            self.view.addSubview(button)

            button.setAttributedTitle(myNormalAttributedTitle, for: .normal)

            // all constaints
            let widthContraints =  NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: CGFloat(layout.accent.block.sizing.medium.minWidth))
            let heightContraints = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: CGFloat(layout.accent.block.sizing.medium.height))
            let xContraints = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0)
            let yContraints = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([heightContraints,widthContraints,xContraints,yContraints])

        case .standard:
            button.layer.borderWidth = 1.0
            button.layer.cornerRadius = CGFloat(layout.default.block.base.borderRadius)
            button.layer.borderColor = UIColor.init(hexString: "#bcbcbc").cgColor
            button.backgroundColor = UIColor(hexString: layout.default.block.interactivity.rest.backgroundColor)

            let myNormalAttributedTitle = NSAttributedString(string: "Button",
                                                             attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: CGFloat(layout.default.text.base.fontSize)),
                                                             NSAttributedString.Key.foregroundColor: UIColor(hexString: layout.default.text.interactivity.rest.fontColor)])

            // add button on view
            self.view.addSubview(button)

            button.setAttributedTitle(myNormalAttributedTitle, for: .normal)

            // all constaints
            let widthContraints =  NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: CGFloat(layout.default.block.sizing.medium.minWidth))
            let heightContraints = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: CGFloat(layout.default.block.sizing.medium.height))
            let xContraints = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0)
            let yContraints = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([heightContraints,widthContraints,xContraints,yContraints])

        }


    }

    func showAnimation () {
        self.loadingView = LoadingView(frame: self.view.frame)
        self.loadingView?.show(view: self.view)
    }
    
    func hideAnimation() {
        self.loadingView?.close()
    }
    
    func showLoader() {
        let loadingView = RSLoadingView()
        loadingView.speedFactor = CGFloat(kITHomeViewControllerLoadingSpeed)
        loadingView.sizeInContainer = kITHomeViewControllerLoadingSize
        loadingView.show(on: self.view)
    }
    
    func hideLoader() {
        RSLoadingView.hide(from: self.view)
    }

    @IBAction func updateButton(_ sender: UISegmentedControl) {
        let type: ButtonType = ButtonType(rawValue: sender.selectedSegmentIndex) ?? .accent
        getButton(type: type)
    }
}
