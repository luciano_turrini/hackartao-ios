//
//  HomeApiProtocol.swift
//  POC-
//
//  Created by Luciano Moreira Turrini on 31/10/17.
//  Copyright © 2017 CI&T. All rights reserved.
//

import UIKit

protocol HomeApiProtocol {

    func getButton(completion: @escaping (Data?) -> Void)
    
}

