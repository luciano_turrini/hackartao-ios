//
//  HomeApiProvider.swift
//  POC-
//
//  Created by Luciano Moreira Turrini on 24/10/17.
//  Copyright © 2017 CI&T. All rights reserved.
//

import UIKit

class HomeApiProvider: ApiProvider {

    enum ButtonDesign: Int {
        case windows10
        case material
    }

    func getButton(type: ButtonDesign, completion: @escaping (Data?) -> Void) {
        var url: String = "material"
        switch type {
        case .windows10:
            url = "windows_10"
        case .material:
            url = "material"
        }

        ApiProvider.getWith(urlExtension: url, successBlock: { (data) in
            completion(data)
        }) { (error) in
            completion(nil)
        }
    }
    
}
