//
//  ICApiProvider.swift
//  auCartoes
//
//  Created by Luciano Moreira Turrini on 24/10/2017.
//  Copyright © 2017 CI&T. All rights reserved.
//

import UIKit
import Alamofire

/// Default Completion Callback

typealias SuccessCallback = (_ data: Data) -> Swift.Void
typealias FailureCallback = (_ error: Error) -> Swift.Void

/// Api Provider to manage backend access
internal class ApiProvider {
    
    fileprivate static let kIApiProviderUrl = "http://localhost:3000/"
    
    // MARK: - Initializers
    
    init() { }
    
    // MARK: - Static Methods
    static func getWith(urlExtension: String, successBlock: @escaping SuccessCallback, failureBlock: @escaping FailureCallback) {
        Alamofire.request(kIApiProviderUrl + urlExtension).responseData { (data) in
            let result = data.result
            switch (result) {
            case .success(let data):
                successBlock(data)
                break
            case .failure(let error):
                failureBlock(error)
                break
            }
        }
    }
    
    // MARK: - Static Methods
    static func postWith(urlExtension: String, params: Dictionary<String, Any>, successBlock: @escaping SuccessCallback, failureBlock: @escaping FailureCallback) {
        Alamofire.request(kIApiProviderUrl + urlExtension, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).responseData { (data) in
            let result = data.result
            switch (result) {
            case .success(let data):
                successBlock(data)
                break
            case .failure(let error):
                failureBlock(error)
                break
            }
        }
    }
}
