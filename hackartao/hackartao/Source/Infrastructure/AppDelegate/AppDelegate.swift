//
//  AppDelegate.swift
//  POC-
//
//  Created by Bruno Marçal Lacerda Fonseca on 23/10/17.
//  Copyright © 2017 CI&T. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
}

