//
//  Section.swift
//  POC-
//
//  Created by Cleber Magalhaes Dias on 29/11/17.
//  Copyright © 2017 CI&T. All rights reserved.
//

import UIKit

enum SectionType: Int {
    case ToHelp
    case ToDo
    case ToKnow
    case ToThink
}
