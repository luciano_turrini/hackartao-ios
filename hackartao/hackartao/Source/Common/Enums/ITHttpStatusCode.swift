//
//  HttpStatusCode.swift
//  POC-
//
//  Created by Luciano Moreira Turrini on 31/10/17.
//  Copyright © 2017 CI&T. All rights reserved.
//

import UIKit

enum HttpStatusCode: Int {
    case Ok = 200
    case Created = 201
    case Accepted = 202
    case PartialContent = 206
    case BadRequest = 400
    case Unauthorized = 401
    case Forbidden = 403
    case NotFound = 404
    case UnprocessableEntity = 422
    case InternalServerError = 500
    case NotImplemented = 501
    case BadGateWay = 502
    case ServiceUnavailable = 503
    case ErrorProcessing = 850
    case Unknown = 999
}
