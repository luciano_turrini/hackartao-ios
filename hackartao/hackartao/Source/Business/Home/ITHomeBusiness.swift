//
//  HomeBusiness.swift
//  POC-
//
//  Created by Luciano Moreira Turrini on 24/10/17.
//  Copyright © 2017 CI&T. All rights reserved.
//

import UIKit

struct ButtonTextBase: Codable {
    var fontSize: Int
}

struct ButtonText: Codable {
    var base: ButtonTextBase
    var interactivity: InteractivityText
}

struct ButtonTextState: Codable {
    var fontColor: String
}

struct ButtonState: Codable {
    var backgroundColor: String
}

struct ButtonBase: Codable {
    var borderRadius: Int
}

struct ButtonSizing: Codable {
    var medium: Sizing
}

struct Sizing: Codable {
    var height: Int
    var minWidth: Int
}

struct Interactivity: Codable {
    var rest: ButtonState
}

struct InteractivityText: Codable {
    var rest: ButtonTextState
}

struct ButtonBlock: Codable {
    var base: ButtonBase
    var interactivity: Interactivity
    var sizing: ButtonSizing
}

struct CAButton: Codable {
    var block: ButtonBlock
    var text: ButtonText
}

struct CButton: Codable {
    var `default`: CAButton
    var accent: CAButton
}

enum ButtonType: Int {
    case accent
    case standard
}

class HomeBusiness: BaseBusiness {
    
    var provider: HomeApiProvider!
    
    override init() {
        self.provider = HomeApiProvider()
    }
    
    init(provider: HomeApiProvider) {
        self.provider = provider
    }
    
    func getWindows(completion: @escaping (CButton) -> Void) {
        self.provider.getButton(type: .windows10, completion: { jsonData in
            let button = try! JSONDecoder().decode(CButton.self, from: jsonData ?? Data())
            completion(button)
        })
    }

    func getMaterial(completion: @escaping (CButton) -> Void) {
        self.provider.getButton(type: .material, completion: { jsonData in
            let button = try! JSONDecoder().decode(CButton.self, from: jsonData ?? Data())
            completion(button)
        })
    }

}
