//
//  BaseBusiness.swift
//  POC-
//
//  Created by Luciano Moreira Turrini on 24/10/17.
//  Copyright © 2017 CI&T. All rights reserved.
//

import Foundation

/// enum with errors
///
/// - GenericError:          generic error
/// - ServiceDataParseError: parse error
enum ProviderError: Error {
    case GenericError
    case ServiceDataParseError
}

class BaseBusiness {
    
}
