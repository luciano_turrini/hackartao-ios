//
//  LoadingView.swift
//  POC-
//
//  Created by Diego Francisco Lechado on 08/11/17.
//  Copyright © 2017 CI&T. All rights reserved.
//

import Foundation
import UIKit

class LoadingView: UIView {
    let kWidth = 300
    let kHeight = 230
    let kAnimationName = "preloader"
    let kDelayInSeconds = 3.0
    let kFadeTimeInSeconds = 1.0
    let kBackgroundColorHexStart = 0x0CACA1
    let kBackgroundColorHexEnd = 0x0CACE5
    let kRotationColorAngle:CGFloat = 0.25
    var canClose: Bool = false
    var finishedDelay: Bool = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func show(view: UIView){
        self.backgroundColor = UIColor.white
        self.alpha = 0
        view.addSubview(self)
        
        let animationView: UIActivityIndicatorView = UIActivityIndicatorView()
        animationView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        animationView.center = self.center
        animationView.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        self.addSubview(animationView)
        animationView.startAnimating()
        
        UIView.animate(withDuration: kFadeTimeInSeconds) {
            self.alpha = 1
        }
        
        let delayTime = DispatchTime.now() + kDelayInSeconds
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            self.finishedDelay = true
            self.hide()
        }
    }
    
    func close(){
        self.canClose = true
        self.hide()
    }
    
    func hide(){
        if(self.canClose && self.finishedDelay){
            self.canClose = false
            
            UIView.animate(withDuration: kFadeTimeInSeconds, animations: {
                self.alpha = 0
            }, completion: { (finished) in
                self.removeFromSuperview()
            })
        }
    }
    
    
}
